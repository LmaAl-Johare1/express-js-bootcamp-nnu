const printCount = (count) => {
    console.log('value is ', count)
};

function doAfterSecond (count, callback) {
    setTimeout(callback, 3000 * (count + 1), count);

}

for(let i = 0; i < 10; i++){
    doAfterSecond(i, printCount);
}